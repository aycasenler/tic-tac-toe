package com.example.tictactoegame

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import kotlinx.android.synthetic.main.custom_dialog.view.*
import kotlinx.android.synthetic.main.custom_dialog2.view.*
import kotlinx.android.synthetic.main.custom_dialog2.view.edt2
import kotlin.random.Random
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var btn1: ImageButton? = null
    private var btn2: ImageButton? = null
    private var btn3: ImageButton? = null
    private var btn4: ImageButton? = null
    private var btn5: ImageButton? = null
    private var btn6: ImageButton? = null
    private var btn7: ImageButton? = null
    private var btn8: ImageButton? = null
    private var btn9: ImageButton? = null
    private var singleOrMulti: Boolean = true
    private var winner = -1
    private var tvPlayer1: TextView? = null
    private var tvPlayer2: TextView? = null
    private var tvPlayer1Score: TextView? = null
    private var tvPlayer2Score: TextView? = null


    private var score1 = 0
    private var score2 = 0

    private var player1Name = ""
    private var player2Name = ""

    private var player1 = ArrayList<Int>()
    private var player2 = ArrayList<Int>()
    private var unplayedMoves = ArrayList<Int>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9))

    private var activePlayer = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        customLoginDialog()
    }

    fun init() {
        btn1 = findViewById(R.id.img1)
        btn2 = findViewById(R.id.img2)
        btn3 = findViewById(R.id.img3)
        btn4 = findViewById(R.id.img4)
        btn5 = findViewById(R.id.img5)
        btn6 = findViewById(R.id.img6)
        btn7 = findViewById(R.id.img7)
        btn8 = findViewById(R.id.img8)
        btn9 = findViewById(R.id.img9)

        tvPlayer1 = findViewById(R.id.tvPlayer1)
        tvPlayer2 = findViewById(R.id.tvPlayer2)
        tvPlayer1Score = findViewById(R.id.tvPlayer1Scor)
        tvPlayer2Score = findViewById(R.id.tvPlayer2Scor)

        btn1?.setOnClickListener(this)
        btn2?.setOnClickListener(this)
        btn3?.setOnClickListener(this)
        btn4?.setOnClickListener(this)
        btn5?.setOnClickListener(this)
        btn6?.setOnClickListener(this)
        btn7?.setOnClickListener(this)
        btn8?.setOnClickListener(this)
        btn9?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var cellId = 0
        var btnSelect = v as ImageButton
        when (v.id) {
            btn1?.id -> cellId = 1
            btn2?.id -> cellId = 2
            btn3?.id -> cellId = 3
            btn4?.id -> cellId = 4
            btn5?.id -> cellId = 5
            btn6?.id -> cellId = 6
            btn7?.id -> cellId = 7
            btn8?.id -> cellId = 8
            btn9?.id -> cellId = 9
        }

        if (singleOrMulti)
            singlePlayGame(cellId, btnSelect)
        else
            multiPlayGame(cellId, btnSelect)
    }

    private fun multiPlayGame(cellId: Int, btnSelect: ImageButton) {
        when (activePlayer) {
            1 -> {
                btnSelect.setImageResource(R.drawable.x_icon)
                player1.add(cellId)
                activePlayer = 2


            }
            2 -> {
                btnSelect.setImageResource(R.drawable.o_icon)
                player2.add(cellId)
                activePlayer = 1

            }
        }
        btnSelect.isEnabled = false
        checkWinner()

    }

    private fun singlePlayGame(cellId: Int, btnSelect: ImageButton) {

        when (activePlayer) {
            1 -> {
                btnSelect.setImageResource(R.drawable.x_icon)
                player1.add(cellId)
                unplayedMoves.remove(cellId)
                activePlayer = 2

                checkWinner()

                if (winner == -1)
                    Autoplay()

            }
            2 -> {
                btnSelect.setImageResource(R.drawable.o_icon)
                player2.add(cellId)
                unplayedMoves.remove(cellId)
                activePlayer = 1

                checkWinner()
            }
        }
        btnSelect.isEnabled = false
    }

    private fun checkWinner() {
        //1
        if (player1.contains(1) && player1.contains(2) && player1.contains(3)) {
            winner = 1
        }
        if (player2.contains(1) && player2.contains(2) && player2.contains(3)) {
            winner = 2
        }
        if (player1.contains(1) && player1.contains(4) && player1.contains(7)) {
            winner = 1
        }
        if (player2.contains(1) && player2.contains(4) && player2.contains(7)) {
            winner = 2
        }
        if (player1.contains(1) && player1.contains(5) && player1.contains(9)) {
            winner = 1
        }
        if (player2.contains(1) && player2.contains(5) && player2.contains(9)) {
            winner = 2
        }
        //2
        if (player1.contains(2) && player1.contains(1) && player1.contains(3)) {
            winner = 1
        }
        if (player2.contains(2) && player2.contains(1) && player2.contains(3)) {
            winner = 2
        }
        if (player1.contains(2) && player1.contains(5) && player1.contains(8)) {
            winner = 1
        }
        if (player2.contains(2) && player2.contains(5) && player2.contains(8)) {
            winner = 2
        }
        //3
        if (player1.contains(3) && player1.contains(1) && player1.contains(2)) {
            winner = 1
        }
        if (player2.contains(3) && player2.contains(1) && player2.contains(2)) {
            winner = 2
        }
        if (player1.contains(3) && player1.contains(5) && player1.contains(7)) {
            winner = 1
        }
        if (player2.contains(3) && player2.contains(5) && player2.contains(7)) {
            winner = 2
        }
        if (player1.contains(3) && player1.contains(6) && player1.contains(9)) {
            winner = 1
        }
        if (player2.contains(3) && player2.contains(6) && player2.contains(9)) {
            winner = 2
        }
        //4
        if (player1.contains(4) && player1.contains(1) && player1.contains(7)) {
            winner = 1
        }
        if (player2.contains(4) && player2.contains(1) && player2.contains(7)) {
            winner = 2
        }
        if (player1.contains(4) && player1.contains(5) && player1.contains(6)) {
            winner = 1
        }
        if (player2.contains(4) && player2.contains(5) && player2.contains(6)) {
            winner = 2
        }
        //5
        if (player1.contains(5) && player1.contains(2) && player1.contains(8)) {
            winner = 1
        }
        if (player2.contains(5) && player2.contains(2) && player2.contains(8)) {
            winner = 2
        }
        if (player1.contains(5) && player1.contains(4) && player1.contains(6)) {
            winner = 1
        }
        if (player2.contains(5) && player2.contains(4) && player2.contains(6)) {
            winner = 2
        }
        if (player1.contains(5) && player1.contains(1) && player1.contains(9)) {
            winner = 1
        }
        if (player2.contains(5) && player2.contains(1) && player2.contains(9)) {
            winner = 2
        }
        if (player1.contains(5) && player1.contains(3) && player1.contains(7)) {
            winner = 1
        }
        if (player2.contains(5) && player2.contains(3) && player2.contains(7)) {
            winner = 2
        }
        //6
        if (player1.contains(6) && player1.contains(3) && player1.contains(9)) {
            winner = 1
        }
        if (player2.contains(6) && player2.contains(3) && player2.contains(9)) {
            winner = 2
        }
        if (player1.contains(6) && player1.contains(4) && player1.contains(5)) {
            winner = 1
        }
        if (player2.contains(6) && player2.contains(4) && player2.contains(5)) {
            winner = 2
        }
        //7
        if (player1.contains(7) && player1.contains(1) && player1.contains(4)) {
            winner = 1
        }
        if (player2.contains(7) && player2.contains(1) && player2.contains(4)) {
            winner = 2
        }
        if (player1.contains(7) && player1.contains(3) && player1.contains(5)) {
            winner = 1
        }
        if (player2.contains(7) && player2.contains(3) && player2.contains(5)) {
            winner = 2
        }
        if (player1.contains(7) && player1.contains(8) && player1.contains(9)) {
            winner = 1
        }
        if (player2.contains(7) && player2.contains(8) && player2.contains(9)) {
            winner = 2
        }
        //8
        if (player1.contains(8) && player1.contains(2) && player1.contains(5)) {
            winner = 1
        }
        if (player2.contains(8) && player2.contains(2) && player2.contains(5)) {
            winner = 2
        }
        if (player1.contains(8) && player1.contains(7) && player1.contains(9)) {
            winner = 1
        }
        if (player2.contains(8) && player2.contains(7) && player2.contains(9)) {
            winner = 2
        }
        //9
        if (player1.contains(9) && player1.contains(7) && player1.contains(8)) {
            winner = 1
        }
        if (player2.contains(9) && player2.contains(7) && player2.contains(8)) {
            winner = 2
        }
        if (player1.contains(9) && player1.contains(3) && player1.contains(6)) {
            winner = 1
        }
        if (player2.contains(9) && player2.contains(3) && player2.contains(6)) {
            winner = 2
        }
        if (player1.contains(9) && player1.contains(1) && player1.contains(5)) {
            winner = 1
        }
        if (player2.contains(9) && player2.contains(1) && player2.contains(5)) {
            winner = 2
        }
        if (winner != -1) {
            if (winner == 1) {
                score1++
                scoreCount(score1, score2)
                customDialog(winner)
            } else if (winner == 2) {
                score2++
                scoreCount(score1, score2)
                customDialog(winner)
            }
        } else
            if (player1.size + player2.size == 9) {
                winner = 0
                customDialog(winner)
            }


    }

    private fun Autoplay() {
        var emptyCells = ArrayList<Int>()
        for (cellId in 1..9) {
            if (!(player1.contains(cellId) || (player2.contains(cellId)))) {
                emptyCells.add(cellId)
            }
        }
        if (!emptyCells.isEmpty()) {
            val rand = Random.nextInt(emptyCells.size)
            val cellId = emptyCells[rand]
            val btnSelect: ImageButton
            when (cellId) {
                1 -> btnSelect = btn1!!
                2 -> btnSelect = btn2!!
                3 -> btnSelect = btn3!!
                4 -> btnSelect = btn4!!
                5 -> btnSelect = btn5!!
                6 -> btnSelect = btn6!!
                7 -> btnSelect = btn7!!
                8 -> btnSelect = btn8!!
                9 -> btnSelect = btn9!!
                else -> btnSelect = btn1!!
            }
            stopTouch()
            Handler().postDelayed({
                enableTouch()
                singlePlayGame(cellId, btnSelect)

            }, 500)

        }
        if (emptyCells.isEmpty()) {
            customDialog(0)

        }


    }

    private fun customDialog(winner: Int) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        if (winner == 0) {
            stopTouch()
            mDialogView.txtView.text = "DRAW"
        } else if (winner == 1) {
            stopTouch()
            mDialogView.txtView.text =
                "$player1Name is won the game. $player1Name score : " + score1
        } else if (winner == 2) {
            stopTouch()
            mDialogView.txtView.text =
                "$player2Name is won the game. $player2Name score : " + score2
        }


        mDialogView.exitBtn.setOnClickListener {
            mAlertDialog.dismiss()
            finish()
        }
        mDialogView.playBtn.setOnClickListener {
            mAlertDialog.dismiss()
            restartGame()
        }
        mDialogView.menuBtn.setOnClickListener {
            score1 = 0
            score2 = 0
            mAlertDialog.dismiss()
            restartGame()
            customLoginDialog()
            tvPlayer1Score?.text = score1.toString()
            tvPlayer2Score?.text = score2.toString()
        }
    }

    fun customLoginDialog() {
        val mDiaglogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog2, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDiaglogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)

        fun startGame() {
            player1Name = mDiaglogView.edt1.text.toString()
            player2Name = mDiaglogView.edt2.text.toString()

            tvPlayer1?.text = player1Name
            tvPlayer2?.text = player2Name

            if(singleOrMulti){
                tvPlayer2?.text = "Computer"
                player2Name = tvPlayer2?.text as String
                if (player1Name.isEmpty()) {
                    tvPlayer1?.text = "You"
                    player1Name = tvPlayer1?.text as String
                }
            }else {
                if (player1Name.isEmpty()) {
                    tvPlayer1?.text = "Player 1"
                    player1Name = tvPlayer1?.text as String
                }

                if (player2Name.isEmpty()) {
                    if (!singleOrMulti) {
                        tvPlayer2?.text = "Player 2"
                        player2Name = tvPlayer2?.text as String
                    }
                }
            }
            mAlertDialog.dismiss()
        }

        mDiaglogView.multiBtn.setOnClickListener {
            mDiaglogView.edt2?.visibility = View.VISIBLE
            mDiaglogView.edt1?.visibility = View.VISIBLE
            mDiaglogView.startGameMulti?.visibility = View.VISIBLE
        }

        mDiaglogView.singleBtn.setOnClickListener {
            mDiaglogView.edt1?.visibility = View.VISIBLE
            mDiaglogView.startGameSingle?.visibility = View.VISIBLE
        }

        mDiaglogView.startGameSingle.setOnClickListener {
            singleOrMulti = true
            startGame()
        }

        mDiaglogView.startGameMulti.setOnClickListener {
            singleOrMulti = false
            startGame()

        }
    }

    fun scoreCount(score1: Int, score2: Int) {
        tvPlayer1Score?.text = "Score: $score1"
        tvPlayer2Score?.text = "Score: $score2"
    }

    fun stopTouch() {
        btn1?.isEnabled = false
        btn2?.isEnabled = false
        btn3?.isEnabled = false
        btn4?.isEnabled = false
        btn5?.isEnabled = false
        btn6?.isEnabled = false
        btn7?.isEnabled = false
        btn8?.isEnabled = false
        btn9?.isEnabled = false
    }

    fun restartGame() {
        btn1?.setImageResource(R.drawable.empty_icon)
        btn2?.setImageResource(R.drawable.empty_icon)
        btn3?.setImageResource(R.drawable.empty_icon)
        btn4?.setImageResource(R.drawable.empty_icon)
        btn5?.setImageResource(R.drawable.empty_icon)
        btn6?.setImageResource(R.drawable.empty_icon)
        btn7?.setImageResource(R.drawable.empty_icon)
        btn8?.setImageResource(R.drawable.empty_icon)
        btn9?.setImageResource(R.drawable.empty_icon)

        player1Name = ""
        player2Name = ""
        player1.clear()
        player2.clear()
        activePlayer = 1

        winner = -1
        unplayedMoves = ArrayList<Int>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9))
        btn1?.isEnabled = true
        btn2?.isEnabled = true
        btn3?.isEnabled = true
        btn4?.isEnabled = true
        btn5?.isEnabled = true
        btn6?.isEnabled = true
        btn7?.isEnabled = true
        btn8?.isEnabled = true
        btn9?.isEnabled = true
    }

    fun enableTouch() {
        for (i in unplayedMoves) {
            when (i) {
                1 -> btn1?.isEnabled = true
                2 -> btn2?.isEnabled = true
                3 -> btn3?.isEnabled = true
                4 -> btn4?.isEnabled = true
                5 -> btn5?.isEnabled = true
                6 -> btn6?.isEnabled = true
                7 -> btn7?.isEnabled = true
                8 -> btn8?.isEnabled = true
                9 -> btn9?.isEnabled = true
            }
        }
    }
}